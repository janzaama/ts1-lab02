package cz.cvut.fel.ts1;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class janzaamaTest {
    @Test
    public void factorialRecursiveTest() {
        janzaama janzaamaInstance = new janzaama();
        assertEquals(1, janzaama.factorialRecursive(0));
        assertEquals(1, janzaama.factorialRecursive(1));
        assertEquals(2, janzaama.factorialRecursive(2));
        assertEquals(6, janzaama.factorialRecursive(3));
        assertEquals(24, janzaama.factorialRecursive(4));
        assertEquals(120, janzaama.factorialRecursive(5));
    }
    @Test
    public void factorialIterativeTest() {
        janzaama janzaamaInstance = new janzaama();
        assertEquals(1, janzaama.factorialIterative(0));
        assertEquals(1, janzaama.factorialIterative(1));
        assertEquals(2, janzaama.factorialIterative(2));
        assertEquals(6, janzaama.factorialIterative(3));
        assertEquals(24, janzaama.factorialIterative(4));
        assertEquals(120, janzaama.factorialIterative(5));
    }
}
